from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from donationform.models import DonateFormObj2
import urllib.request, json
import requests

def history(request):
    return render(request, 'history.html')

def get_history(request):
    total = 0
    array = []
    donaturs = DonateFormObj2.objects.all().filter(email = request.session['userEmail'])
    for i in donaturs:
        data = {}
        data['title'] = i.title_program
        data['nominal'] = i.nominal
        array.append(data)
        total += i.nominal
    return JsonResponse({"dataku" : array, "nominal_total": total})
    


