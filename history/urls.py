from django.urls import path
from .views import history, get_history

urlpatterns = [
    path('history/', history, name='history'),
    path('get_history/', get_history, name='get_history'),
]