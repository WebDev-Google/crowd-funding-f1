from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from history.views import history, get_history
from donationform.models import DonateFormObj2
from .apps import HistoryConfig
from django.apps import apps

class HistoryUnitTest(TestCase): 
           
    def test_history_url_is_exist(self):
        response = Client().get('/riwayat/history/')
        self.assertEqual(response.status_code, 200)

    def test_history_using_func(self):
        found = resolve('/riwayat/history/')
        self.assertEqual(found.func, history)

    def test_history_using_template(self):
        response = Client().get('/riwayat/history/')
        self.assertTemplateUsed(response, 'history.html')

    def test_tugas_1_returns_correct_html(self):
        request = HttpRequest()
        response = history(request)
        html = response.content.decode('utf8')
        self.assertIn('We owe you big thanks. See how much you participate in helping other!', html)

    def test_model_can_create_all(self):
        # Creating a new activity
        object1 =  DonateFormObj2.objects.create(name="Nama Saya", email="a@gmail.com", nominal=100000)

        # Retrieving all available activity
        counting_all_status = DonateFormObj2.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    # def test_form_post_and_render_the_result(self):
    #     nama_program = 'Bersama, Bantu Palu, Donggala'
    #     nama = 'Anonymous'
    #     email = "test@sinarperak.com"
    #     jumlah_uang = 100000
    #     dukungan = "Semangat !"
    #     response_post = Client().post('/riwayat/get_history',
    #                                   {'nama_program' : nama_program,'nama': nama, 'email': email, 'jumlah_uang': jumlah_uang, 'dukungan': dukungan, 'cekAnon':{}})
    #     self.assertEqual(response_post.status_code, 302)

