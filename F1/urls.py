"""F1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import RedirectView

from django.conf import settings
from django.conf.urls.static import static

from about.views import about

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls')),
    path('registrasi/', include('regist.urls')),
    path('donasi/', include('donationform.urls')),
    path('beritaLengkap/', include('spesificnews.urls')),
    path('program/', include('programs.urls')),
    path('berita/', include('news.urls')),
    path('riwayat/', include('history.urls')),
    path('about/', include('about.urls')),
    path('about/', about, name='about'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
