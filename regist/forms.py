from django import forms
from .models import Account
from django.forms.utils import ErrorList

class AccountForm(forms.Form):
    FullName = forms.CharField(max_length=50)
    Address = forms.CharField(max_length=50)
    Email = forms.EmailField(max_length=50)
    Phone = forms.CharField(max_length=50)
    Occupation = forms.CharField(max_length=50)
    Institute = forms.CharField(max_length=50)