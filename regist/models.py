from django.db import models

# tidak dipakai lagi, peninggalan TP1
class Account(models.Model):
    FullName = models.CharField(max_length=50)
    Address = models.CharField(max_length=50)
    Email = models.EmailField(max_length=50)
    Phone = models.CharField(max_length=50)
    Occupation = models.CharField(max_length=50)
    Institute = models.CharField(max_length=50)