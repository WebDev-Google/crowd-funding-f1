from django.contrib.sessions.middleware import SessionMiddleware
from django.test import TestCase, Client
from django.http import HttpRequest
from .forms import AccountForm
from .models import Account
from .views import registPage, session, userControl, verify


class AccountTest(TestCase):
    def test_page_is_exist(self):
        response = Client().get('/registrasi/regist/')
        self.assertEqual(response.status_code, 200)

    def test_page_using_template(self):
        response = Client().get('/registrasi/regist/')
        self.assertTemplateUsed(response, 'regist.html')

    def test_render_page_content(self):
        request = HttpRequest()
        response = registPage(request)
        htmlResponse = response.content.decode('utf8')
        self.assertIn('register', htmlResponse)

    def test_using_navbar(self):
        response = Client().get('/registrasi/regist/')
        self.assertTemplateUsed(response, 'partial/navbar.html')

    def test_using_base(self):
        response = Client().get('/registrasi/regist/')
        self.assertTemplateUsed(response, 'layout/base.html')

    def test_using_static_file_style(self):
        request = HttpRequest()
        response = registPage(request)
        htmlResponse = response.content.decode('utf8')
        self.assertIn('/static/css', htmlResponse)

    def test_using_static_file_script(self):
        request = HttpRequest()
        response = registPage(request)
        htmlResponse = response.content.decode('utf8')
        self.assertIn('/static/js', htmlResponse)

    def test_google_api_active(self):
        request = HttpRequest()
        response = registPage(request)
        htmlResponse = response.content.decode('utf8')
        self.assertIn('apis.google.com', htmlResponse)

    def test_registPage_form_invalid(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['Email'] = "asdfasfasd"
        response = registPage(request)
        self.assertEqual(response.status_code, 302)

    def test_registPage_form_valid_can_made_object(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['FullName'] = "Nathanael"
        request.POST['Address'] = "on earth"
        request.POST['Email'] = "evo220999@gmail.com"
        request.POST['Phone'] = "089503386642"
        request.POST['Occupation'] = "student"
        request.POST['Institute'] = "NewbieWorks"
        response = registPage(request)
        self.assertEqual(response.status_code, 302)

    def test_form_available(self):
        form = AccountForm()
        self.assertIn("email", form.as_p())

    def test_form_empty_input(self):
        form = AccountForm({'email': ''})
        self.assertFalse(form.is_valid())

    def test_model_available(self):
        objective = Account(FullName="Nathanael", Address="On earth",
                            Email="evo220999@gmail.com", Phone="0895033866422",
                            Occupation="Student", Institute="NewbieWorks")
        objective.save()
        self.assertEquals(Account.objects.all().count(), 1)

    def test_verify_no_Post(self):
        request = HttpRequest()
        request.method = "GET"
        self.assertIsNone(verify(request))

    def test_verify_fail_post(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['token'] = "123456789"
        response = verify(request)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'status': '1'}
        )

    def test_session_can_add(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['emailUrl'] = "evo220999@gmail.com"
        request.POST['name'] = "Nathanael"
        request.POST['givenName'] = "Nathanael"
        request.POST['avatar'] = "google.com"
        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()
        response = session(request)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 302)

    def test_user_Control_working(self):
        response = Client().get('/registrasi/userControl')
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'logedIn': False}
        )