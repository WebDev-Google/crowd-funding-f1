from django.urls import path
from .views import registPage, session, verify, userControl, logout

urlpatterns = [
    path('regist/', registPage, name='registPage'),
    path('verifyMe', verify, name='verify'),
    path('session', session, name='toSession'),
    path('userControl/logout', logout, name='logout'),
    path('userControl', userControl, name='userControl')
]