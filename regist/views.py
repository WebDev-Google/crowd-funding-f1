from django.shortcuts import render, HttpResponseRedirect
from django.http import JsonResponse, HttpResponse
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import logout as lout
from django.contrib.auth import login as lin
from google.oauth2 import id_token
from google.auth.transport import requests
from .forms import AccountForm
from .models import Account

def registPage(request):
    html = 'regist.html'
    context = {
        'registForm': AccountForm(),
    }
    if (request.method == "POST"):
        form = AccountForm(request.POST)
        if (form.is_valid()):
            response = {}
            response['FullName'] = request.POST['FullName']
            response['Address'] = request.POST['Address']
            response['Email'] = request.POST['Email']
            response['Phone'] = request.POST['Phone']
            response['Occupation'] = request.POST['Occupation']
            response['Institute'] = request.POST['Institute']
            objective = Account(FullName=response['FullName'], Address=response['Address'], 
                                Email=response['Email'], Phone=response['Phone'], 
                                Occupation=response['Occupation'], Institute=response['Institute'])
            objective.save()
        return HttpResponseRedirect('../index')
    return render(request, html, context)
    
def verify(request):
    if (request.method == "POST"):
        response = {
            'status' : "0"
        }
        try:
            toVerify = request.POST['token']
            info = id_token.verify_oauth2_token(toVerify, requests.Request(),"787579258880-de1h5msk69rtp8rbn6jjhp7hsdd4chb1.apps.googleusercontent.com")
            if (info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']):
                raise ValueError('Wrong issuer.')
        except ValueError:
            response["status"] = "1"
        return JsonResponse(response)

# Simpan session setelah login ke dalam object model (isi hanya email) atau get object jika sudah ada
# jangan lupa untuk save object jika ada perubahan (tidak akan ada perubahan di session user, tapi di model donation)
def session(request):
    if (request.method == "POST"):
        if not (User.objects.filter(email=request.POST['emailUrl']).exists()):
            user = User.objects.create_user(username=request.POST["name"], email=request.POST["emailUrl"])
            user.save()
            lin(request, user)
        else:
            user = User.objects.get(email=request.POST['emailUrl'])
            user.save()
            lin(request, user)
        request.session['userEmail'] = request.POST["emailUrl"]
        request.session['userName'] = request.POST["name"]
        request.session['userGivenName'] = request.POST["givenName"]
        request.session['userAvatar'] = request.POST["avatar"]
        return HttpResponseRedirect(reverse('index'))

# untuk cek apa user sudah log in atau belum, dipakai level projek (untuk ajax)
# untuk views, gunakan 'userEmail' in request.session.keys()
def userControl(request):
    response = {}
    if ('userEmail' in request.session.keys()):
        response['logedIn'] = True
        response['name'] = request.session['userGivenName']
    else:
        response['logedIn'] = False
    return JsonResponse(response)

# untuk logout, dipakai level projek
def logout(request):
    request.session.flush()
    response = {}
    response['logedOut'] = True
    lout(request)
    return JsonResponse(response)

