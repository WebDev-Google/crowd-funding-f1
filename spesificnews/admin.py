from django.contrib import admin
from .models import Data, SpecNewsModel

# Register your models here.
admin.site.register(Data)
admin.site.register(SpecNewsModel)
