from django.db import models

# Create your models here.
class Data (models.Model):
	TOTAL = models.CharField(max_length=50)
	Name = models.CharField(max_length=50)
	Date = models.DateField()

class SpecNewsModel(models.Model):
	image = models.CharField(max_length=300)
	date = models.DateField()
	title = models.CharField(max_length=200)
	desc = models.TextField(max_length=500)

