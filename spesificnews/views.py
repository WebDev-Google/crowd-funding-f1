from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import Data
from news.models import NewsModel

# Create your views here.
response = {}

def spesific_news(request, title):
		response['listnews'] = NewsModel.objects.all().filter(title=title)
		response['donatur'] = Data.objects.all()
		return render(request, 'spesificnews.html', response)