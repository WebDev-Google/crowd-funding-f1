from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import spesific_news
from .models import Data

# Create your tests here.
class SpesificNewsTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/beritaLengkap/spesificnews/')
        self.assertEqual(response.status_code, 200)

    def test_using_spesificnews_func(self):
        found = resolve('/beritaLengkap/spesificnews/')
        self.assertEqual(found.func, spesific_news)
    
    #def test_headline_is_written(self):
     #   response = self.client.get('/spesificnews/')
      #  string = response.content.decode("utf-8")
       # self.assertIn("Hope All but Gone, Keeping Vigil for the Missing in the Indonesian Earthquake", string)
    
    #def test_model_can_create_new_data(self):
        #Creating a new data
    #    new_data = Data.objects.create(TOTAL='Rp750.000,00', Name='Kak Pewe')

        #Retrieving all available data
     #   counting_all_available_data = Data.objects.all().count()
      #  self.assertEqual(counting_all_available_data, 1)