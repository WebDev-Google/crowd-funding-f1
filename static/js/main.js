$(function() {
    $(document).scroll(function() {
        var $nav = $(".fixed-top");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
    });
});

// card nav
$num = $('.my-card').length;
$even = $num / 2;
$odd = ($num + 1) / 2;

if ($num % 2 == 0) {
    $('.my-card:nth-child(' + $even + ')').addClass('active');
    $('.my-card:nth-child(' + $even + ')').prev().addClass('prev');
    $('.my-card:nth-child(' + $even + ')').next().addClass('next');
} else {
    $('.my-card:nth-child(' + $odd + ')').addClass('active');
    $('.my-card:nth-child(' + $odd + ')').prev().addClass('prev');
    $('.my-card:nth-child(' + $odd + ')').next().addClass('next');
}

$('.my-card').hover(function() {
    $slide = $('.active').width();
    console.log($('.active').position().left);

    if ($(this).hasClass('next')) {
        $('.card-carousel').stop(false, true).animate({
            left: '-=' + $slide
        });
    } else if ($(this).hasClass('prev')) {
        $('.card-carousel').stop(false, true).animate({
            left: '+=' + $slide
        });
    }

    $(this).removeClass('prev next');
    $(this).siblings().removeClass('prev active next');

    $(this).addClass('active');
    $(this).prev().addClass('prev');
    $(this).next().addClass('next');
});

(function($) {
    $(function() {

        $('.parallax').parallax();

    }); // end of document ready
})(jQuery); // end of jQuery name space

$(function() {

    // animate
    $(window).scroll(function() {
        $('.animateMePlz').animateMePlz({
            animationStyle: 'fadeInUp',
            animationDelay: '.5s',
            animationDuration: '.5s',
            repeat: 'noRepeat',
            threshold: 50 // 50px
        });
        $('.animateMePlzNoThres').animateMePlz({
            animationStyle: 'fadeInUp',
            animationDelay: '.5s',
            animationDuration: '.5s',
            repeat: 'noRepeat'
        });
        $('.animateMePlzBlank').animateMePlz({
            animationStyle: 'fadeIn',
            animationDelay: '.5s',
            animationDuration: '.5s',
            repeat: 'noRepeat',
            threshold: 50 // 50px
        });
        $('.animateMePlzPulse').animateMePlz({
            animationStyle: 'pulse',
            repeat: 'repeat',
            threshold: 50 // 50px
        });
    });

    // digit scroll
    var toscroll = document.getElementById("love").innerText
    document.getElementById("love").innerText = toscroll - 500;
    var scroller = $("#love").digitScroller({
        animationEnd: function() { // fired when digit scrolling animation is done
            document.getElementById("love").innerText = "Rp " + toscroll;
        }
    }).scrollTo(toscroll);

    var toscroll2 = document.getElementById("helpneed").innerText
    document.getElementById("helpneed").innerText = toscroll2 - 600;
    var scroller = $("#helpneed").digitScroller({}).scrollTo(toscroll2);

    var toscroll3 = document.getElementById("helper").innerText
    document.getElementById("helper").innerText = toscroll3 - 700;
    var scroller = $("#helper").digitScroller({
        animationEnd: function() {
            document.getElementById("helper").innerText = "" + toscroll3;
        }
    }).scrollTo(toscroll3);
});