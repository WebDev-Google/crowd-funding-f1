// butuh user control yang oper lewat ajax
// ajax url ke urls.py milik regist
// ke views nya regist untuk minta status login dan nama panggilan
// ganti button registet menjadi nama dengan drop down menu untuk log out (dan page history donasi)
// tidak perlu on document ready, tapi harus on navbar load, jadi langsung saja ..

// load 
function onLoad() {
    gapi.load('auth2', function() {
        gapi.auth2.init();
        console.log("loaded");
    });
}

$.ajax({
    url: "/registrasi/userControl",
    success: function(result) {
        console.log(result);
        if (result['logedIn']) {
            // build dropdown
            var replacator = "<li class='nav-item dropdown'>";
            replacator += "<a class='nav-link dropdown-toggle' href='#' id='accountDropdown' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
            replacator += "Hello, ";
            replacator += result['name'];
            replacator += "</a>";
            replacator += "<div class='dropdown-menu' aria-labelledby='accountDropdown'>";
            replacator += "<a class='dropdown-item' href='#' onclick='toProfile()'>";
            replacator += "My Profile";
            replacator += "</a>";
            replacator += "<a class='dropdown-item' href='#' onclick='logout()'>";
            replacator += "Log out";
            replacator += "</a>";
            replacator += "</div>";
            replacator += "</li>";

            // ganti button registrasi
            $("#registButton").replaceWith(replacator);
        }
        // else do nothing
    }
})