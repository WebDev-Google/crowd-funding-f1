$(document).ready(function () {
    $.ajax({
    url: "testimony_data",
    datatype: 'json',
    success: function(data){
        $('#testi').html('')
        var res ='';
        for(var i = 0; i < data.datas.length; i++) {
            res+= '<div class="card" id="card"><div class="card-header">They said</div><div class="card-body"><blockquote class="blockquote mb-0"><p>'+data.datas[i].testimony+'</p><footer class="blockquote-footer">'+data.datas[i].name+'<cite title="Source Title"></cite></footer></blockquote></div></div>'
        }
        $('#testi').append(res);
    },
    error: function(error){
        alert("There's no any testimony yet");
    }
})

var form = $('#field')
form.submit( function() {
        $testimony_input = $('#testimony-form').val();
            $.ajax({
                type: "POST",
                url : "",
                data: {
                    sent: $testimony_input,
                    csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                },
                success: function(){
                    alert("Thank you for your testimony!");
                    $('#id_testimony_form').val('');
                }
            });
    });

});