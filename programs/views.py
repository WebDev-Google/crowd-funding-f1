from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import ProgramModel

response = {}

# Create your views here.
def programs(request):
    listprogram = ProgramModel.objects.all()
    if 'userEmail' in request.session.keys() and not (request.session['userEmail'] is None):
        response['logged'] = True
    else:
        response['logged'] = False
    response['listprogram'] = listprogram
    return render(request, 'programs.html', response)
