from django.db import models

# Create your models here.
class ProgramModel(models.Model):
    image = models.CharField(max_length=300)
    title = models.CharField(max_length=200)

    def __str__(self):
        return self.title
