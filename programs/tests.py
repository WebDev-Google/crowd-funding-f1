from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import programs
from .models import ProgramModel

# Create your tests here.
class Tugas1UnitTest(TestCase):
    def test_programs_url_is_exist(self):
        response = Client().get('/program/programs')
        self.assertEqual(response.status_code, 200)

    def test_programs_using_programs_function(self):
        found = resolve('/program/programs')
        self.assertEqual(found.func, programs)
    
    def test_model_can_add_new(self):
        ProgramModel.objects.create(image="test", title="test")
        program_counts_all_entries = ProgramModel.objects.all().count()
        self.assertEqual(program_counts_all_entries, 1)

    def test_self_func_programs(self):
        ProgramModel.objects.create(image="test", title="test")
        program = ProgramModel.objects.get(title="test")
        self.assertEqual(str(program), program.title)

    def test_donateform_using_template(self):
        session = self.client.session
        session['userEmail'] = 'userEmail'
        session.save()
        response = self.client.get('/program/programs')
        self.assertTemplateUsed(response, 'programs.html')
