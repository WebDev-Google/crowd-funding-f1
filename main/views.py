from django.shortcuts import render
from django.http import HttpResponseRedirect
from news.models import NewsModel
from programs.models import ProgramModel
from django.contrib.auth.models import User


quote1 = "\"Make The World a Better Place\""
quote2 = "\"For You and For Me and The Entire Human Race\""
quotator = "-Michael Jackson"
lovers = "1000000"
helpneed = "100000"
description = "Natural disasters such as earthquakes, tsunamis, and floods come at the least expected time. Others, such as hurricanes and cyclones are increasing in severity and destruction. No one could predict when and where exactly Natural disasters would come. The poor are the worst hit for they have the least resources to cope and rebuild. The Victims of Natural Disasters need help,  as big as help needed when we became the victim ourself. Donating a little bit of money or time may not seem like much. But if your donation is joined with others, it becomes something much bigger."

def index(request):
    html = 'homepage.html'
    if (ProgramModel.objects.all().count() < 4):
        recentProgramPassed = ProgramModel.objects.all()
    else :
        recentProgramPassed = ProgramModel.objects.all()[ProgramModel.objects.count() - 3::]
    if (NewsModel.objects.all().count() > 6):
        toCarousel = NewsModel.objects.all()[NewsModel.objects.count() - 3::]
        recentNewsPassed = NewsModel.objects.all()[NewsModel.objects.count() - 6:NewsModel.objects.count() - 3:]
    elif (NewsModel.objects.all().count() < 3):
        toCarousel = NewsModel.objects.all()
        recentNewsPassed = toCarousel
    else :
        toCarousel = ''
        recentNewsPassed = ''
    helper = User.objects.count()

    content = {
        'quote1' : quote1,
        'quote2' : quote2,
        'quotator' : quotator,
        'lovers' : lovers,
        'helper' : helper,
        'helpneed' : helpneed,
        'description' : description,
        'toCarousel' : toCarousel,
        'recentProgramPassed' : recentProgramPassed,
        'recentNewsPassed' : recentNewsPassed
    }
    return render(request, html, content)
