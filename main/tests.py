from django.test import TestCase, Client
from django.http import HttpRequest
from .views import index

class HomePageTest(TestCase):

    def test_homepage_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_homepage_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage.html')

    def test_homepage_content(self):
        request = HttpRequest()
        response = index(request)
        htmlResponse = response.content.decode('utf8')
        self.assertIn('Why Donate ?', htmlResponse)

    def test_carousels_loaded(self):
        request = HttpRequest()
        response = index(request)
        htmlResponse = response.content.decode('utf8')
        self.assertIn('carousel', htmlResponse)

    def test_using_navbar(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'partial/navbar.html')

    def test_has_footer(self):
        request = HttpRequest()
        response = index(request)
        htmlResponse = response.content.decode('utf8')
        self.assertIn('<footer', htmlResponse)

    def test_using_base(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'layout/base.html')

    def test_using_static_file_style(self):
        request = HttpRequest()
        response = index(request)
        htmlResponse = response.content.decode('utf8')
        self.assertIn('/static/css', htmlResponse)

    def test_using_static_file_script(self):
        request = HttpRequest()
        response = index(request)
        htmlResponse = response.content.decode('utf8')
        self.assertIn('/static/js', htmlResponse)

    def test_google_api_active(self):
        request = HttpRequest()
        response = index(request)
        htmlResponse = response.content.decode('utf8')
        self.assertIn('apis.google.com', htmlResponse)