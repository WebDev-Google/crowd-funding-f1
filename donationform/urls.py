from django.urls import path
from .views import thanks2, formfunction2

app_name = 'donationform'

urlpatterns = [
    path('thanks2/', thanks2, name='thanks2'),
    path('formfunction2/<str:title_id>/', formfunction2, name='formfunction2'),
]