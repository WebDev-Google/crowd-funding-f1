from django import forms 

class DonateForm2(forms.Form):
    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Full name'
    }
    email_attrs = {
        'type': 'email',
        'class': 'form-control',
        'placeholder': 'Input valid email address, please.',
    }
    nominal_attrs = {
        'type': 'number',
        'class': 'form-control',
        'placeholder': 'How much do you want to donate?'
    }
    name = forms.CharField(label="Name", max_length=300, required=True, widget=forms.TextInput(attrs=name_attrs))
    email = forms.EmailField(label='E-mail', required=True, widget=forms.EmailInput(attrs=email_attrs))
    nominal = forms.IntegerField(label='Nominal', required=True, min_value=1, widget=forms.NumberInput(attrs=nominal_attrs))
    anonymous = forms.CharField(label='Anonymous?', required=False, widget=forms.CheckboxInput())