from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from .forms import DonateForm2
from .models import DonateFormObj2

response = {}

def formfunction2(request, title_id):
    # cek method post atau bukan
    if(request.method == 'POST'):
        if 'anonymous' in request.POST:
            response['name'] = 'Anonymous'
        else:
            response['name'] = request.POST['name']
        response['nominal'] = request.POST['nominal']
        email = request.POST['email']
        #response['email'] = request.session['userEmail']
        myform = DonateFormObj2(name=response['name'], email=email, nominal=response['nominal'], title_program=title_id)
        myform.save()
        return HttpResponseRedirect('/donasi/thanks2')
    # kalau kagak, cek udah login apa belum
    if 'userEmail' in request.session.keys(): 
        # kalau sudah, lanjut
        response['listdonations'] = DonateFormObj2.objects.all().filter(title_program = title_id)
        response['title'] = title_id
        response['listform'] = DonateForm2
        response['email_lengkap'] = request.session['userEmail']
        return render(request, 'donate_form2.html', response)
    # kalau belum, redirect ke main page
    return HttpResponseRedirect('/')


def thanks2(request):
    return render(request, 'thanks2.html', response)