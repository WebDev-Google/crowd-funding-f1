from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import formfunction2, thanks2
from .models import DonateFormObj2
from .forms import DonateForm2

class DonateFormUnitTest2(TestCase):

    # DONATION-FORM PAGE
    def test_donateform_url_is_exist(self):
        response = Client().get('/donasi/formfunction2/<str:title_id>/')
        self.assertEqual(response.status_code, 302)
    
    def test_donateform_using_formfunction_func(self):
        found = resolve('/donasi/formfunction2/<str:title_id>/')
        self.assertEqual(found.func, formfunction2)

    def test_donateform_using_template(self):
        session = self.client.session
        session['userEmail'] = 'email_lengkap'
        session.save()
        response = self.client.get('/donasi/formfunction2/<str:title_id>/')
        self.assertTemplateUsed(response, 'donate_form2.html')

    def test_model_can_save_data_form(self):
        data = DonateFormObj2.objects.create(name="aku", nominal="100")
        counting_all_data = DonateFormObj2.objects.all().count()
        self.assertEqual(counting_all_data, 1)

    def test_self_func_name(self):
        DonateFormObj2.objects.create(name="aku", nominal="100")
        donor = DonateFormObj2.objects.get(nominal='100')
        self.assertEqual(100, donor.nominal)

    def test_if_form_is_blank(self):
        form = DonateForm2(data={
            'name': '',
            'nominal': ''
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'][0],
            'This field is required.',
        )
    
    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = DonateForm2()
        self.assertIn('class="form-control', form.as_p())
    
    def test_form_session(self):
        # session = self.client.session
        # session['userEmail'] = "abc@gmail.com"
        # session.save()
        response_post = Client().post('/donasi/formfunction2/<str:title_id>/', {'name': "abc", 'email': "abc@gmail.com", 'nominal': 123, 'anonymous': True})
        donor = DonateFormObj2.objects.all().count()
        self.assertEqual(donor, 1)

    def test_form_session2(self):
        # session = self.client.session
        # session['userEmail'] = "abc@gmail.com"
        # session.save()
        response_post = Client().post('/donasi/formfunction2/<str:title_id>/', {'name': "abc", 'email': "abc@gmail.com", 'nominal': 123})
        donor = DonateFormObj2.objects.all().count()
        self.assertEqual(donor, 1)

    
    # THANKS PAGE
    def test_thanks_url_is_exist(self):
        response = Client().get('/donasi/thanks2/')
        self.assertEqual(response.status_code, 200)
    
    def test_thanks_using_thanksfunction_func(self):
        found = resolve('/donasi/thanks2/')
        self.assertEqual(found.func, thanks2)

    def test_thanks_using_template(self):
        response = Client().get('/donasi/thanks2/')
        self.assertTemplateUsed(response, 'thanks2.html')

    def test_thanks_contain_title(self):
        request = HttpRequest()
        response = thanks2(request)
        string = response.content.decode("utf8")
        self.assertIn("Sinar Perak Community", string)

