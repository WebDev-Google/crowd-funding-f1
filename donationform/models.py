from django.db import models

class DonateFormObj2(models.Model):
    name = models.CharField(max_length=300)
    email = models.EmailField()
    nominal = models.IntegerField()
    title_program = models.CharField(max_length = 500, default="title")