from django.db import models

# Create your models here.
class NewsModel(models.Model):
    image = models.CharField(max_length=300)
    date = models.DateField()
    title = models.CharField(max_length=200)
    desc = models.TextField(max_length=500)

    def __str__(self):
        return self.title
