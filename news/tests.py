from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import news
from .models import NewsModel

# Create your tests here.
class Tugas1UnitTest(TestCase):
    def test_news_url_is_exist(self):
        response = Client().get('/berita/news/')
        self.assertEqual(response.status_code, 200)

    def test_news_using_news_function(self):
        found = resolve('/berita/news/')
        self.assertEqual(found.func, news)
    
    def test_model_can_add_new(self):
        NewsModel.objects.create(image="test", title="test", date="1999-10-10", desc="test")
        news_counts_all_entries = NewsModel.objects.all().count()
        self.assertEqual(news_counts_all_entries, 1)
    
    def test_self_func_news(self):
        NewsModel.objects.create(image="test", title="test", date="1999-10-10", desc="test")
        news = NewsModel.objects.get(title="test")
        self.assertEqual(str(news), news.title)
