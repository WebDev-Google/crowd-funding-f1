from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import NewsModel

response = {}

# Create your views here.
def news(request):
    listnews = NewsModel.objects.all()
    response['listnews'] = listnews
    return render(request, 'news.html', response)
