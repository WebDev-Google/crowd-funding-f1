# WebDev-Google

## PPW-2018 kelas F kelompok 1

Anggota :

1. Anastasia Greta Elena - 1706043954
2. Rachel Larasati - 1706984713
3. Yasmin Amalia - 1706043720
4. Nathanael - 1706043670 

### Test Coverage

[![Django report](https://gitlab.com/WebDev-Google/crowd-funding-f1/badges/master/coverage.svg)](https://gitlab.com/WebDev-Google/crowd-funding-f1/commits/master)

### Pipeline Status

![Pipeline status](https://gitlab.com/WebDev-Google/crowd-funding-f1/badges/master/build.svg)