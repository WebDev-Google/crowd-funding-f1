from django.db import models

# Create your models here.
class Testimony (models.Model):
    name = models.CharField(max_length=20)
    testimony = models.CharField(max_length=300)