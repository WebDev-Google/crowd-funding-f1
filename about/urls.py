from django.urls import path
from .views import about, testimony_data

app_name = 'about'

urlpatterns = [
    path('about/', about, name='about'),
    # path('add_testimony/', add_testimony, name='add_testimony'),
    path('testimony_data/', testimony_data, name='testimony_data'),
]