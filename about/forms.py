from django import forms

class Testimony_Form(forms.Form):
    error_message = {
        'required': 'Please fill in'
    }

    # name_attrs = {
    #     'type': 'text',
    #     'class': 'form-control',
    #     'placeholder': 'Your Name',
    #     'id': 'name-form'
    # }
    testi_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': "Tell us about your experience!",
        'id': 'testimony-form'
    }

    # name = forms.CharField(label='Name', required=True, max_length=50,
    # widget=forms.TextInput(attrs=name_attrs))
    testimony = forms.CharField(label='Testimony', required=True, max_length=300,
    widget= forms.Textarea(attrs=testi_attrs))