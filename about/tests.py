from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from django.test import Client
from .views import about
from .forms import Testimony_Form
from .models import Testimony
from django.contrib.sessions.middleware import SessionMiddleware

# Create your tests here.
class aboutUnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/about/about/')
        self.assertEqual(response.status_code, 200)

    def test_using_about_func(self):
        found = resolve('/about/about/')
        self.assertEqual(found.func, about)

    def test_content(self):
        self.assertIsNotNone(about)

    def test_contain(self):
        request = HttpRequest()
        response = about(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<title>About</title>', html_response)
    
    def test_form_validation_for_blank_items(self):
        form = Testimony_Form(data={'testimony': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['testimony'],
            ["This field is required."]
        )
    
