from django.shortcuts import render
from .forms import Testimony_Form
from .models import Testimony
import urllib.request, json
import requests
from django.http import JsonResponse
from django.http import HttpResponse

# Create your views here.
response = {}
response2 = {}
def about(request):
    testimonies = Testimony.objects.all()
    response = {"testimony" : testimonies, "form" : Testimony_Form}
    form = Testimony_Form(request.POST or None)
    
    if(request.method == "POST"):
        if(form.is_valid()):
            testimony_form = request.POST["testimony"]
            Testimony.objects.create(name=request.session['userGivenName'], testimony=testimony_form)
            response2['testimony'] = testimony_form
            response2['name'] = request.session['userGivenName']
            return render(request, 'about.html', response)
            # return HttpResponse(json.dumps(response2), content_type='application/json')
        else:
            return render(request, 'about.html', response)
    else:
        response['form'] = form
        return render(request, 'about.html', response)

def testimony_data(request):
    data = list(Testimony.objects.all().values())
    return JsonResponse({'datas': data})